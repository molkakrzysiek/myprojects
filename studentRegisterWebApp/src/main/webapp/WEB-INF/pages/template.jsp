<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${param.title}</title>

    <link rel="stylesheet" href="<c:url value="/resources/css/style.css" />" type="text/css"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/fontello/fontello.css" />" type="text/css"/>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/flick/jquery-ui.css" type="text/css"/>
    <script src="https://code.jquery.com/jquery-3.1.0.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" type="text/javascript"></script>
    <script src="<c:url value="/resources/datepicker-pl.js"/>" type="text/javascript"></script>
</head>
<body>

<div id="container">
    <jsp:include page="/WEB-INF/pages/header.jsp"/>

    <jsp:include page="/WEB-INF/pages/pagesContents/${param.content}.jsp"/>

    <jsp:include page="/WEB-INF/pages/footer.jsp"/>
</div>

</body>
</html>
