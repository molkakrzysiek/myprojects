<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<spring:message code="web.mainController.index.title" var="title"/>
<jsp:include page="/WEB-INF/pages/template.jsp">
    <jsp:param name="content" value="indexContent"/>
    <jsp:param name="userPanel" value="false"/>
    <jsp:param name="title" value="${title}"/>
</jsp:include>
