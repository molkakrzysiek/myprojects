<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="content">
    <p class="ptitle"> Ocena: </p>
    <hr/>

    <div id="userinfo">
        <table class="properties">
            <tr>
                <th class="dn">Ocena</th>
                <td colspan="1">${grade.grade}</td>
            </tr>
            <tr>
                <th class="dn">Przedmiot</th>
                <td colspan="1">${grade.subject}</td>
            </tr>
            <tr>
                <th class="dn">Opis</th>
                <td colspan="1">${grade.gradeDetail}</td>
            </tr>
            <tr>
                <th class="dn">Nauczyciel</th>
                <td colspan="1">${grade.teacherId}</td>
            </tr>
            <tr>
                <th class="dn">Data</th>
                <td colspan="1">${grade.gradeDate}</td>
            </tr>
        </table>
        <div style="clear: both;"></div>
        <sec:authorize access="hasRole('ROLE_STAFF')">
            <div class="button"><a href="/editGrade/${grade.gradeId}" class="buttonlink">Edytuj ocenę</a>
            </div>
        </sec:authorize>
    </div>
    <div style="clear: both;"></div>

</div>