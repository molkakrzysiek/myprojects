<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<spring:message code="web.studentController.all.surname" var="surname"/>
<spring:message code="web.studentController.all.name" var="name"/>
<spring:message code="web.studentController.all.pesel" var="pesel"/>
<spring:message code="web.studentController.all.street" var="street"/>
<spring:message code="web.studentController.all.houseNumber" var="houseNumber"/>
<spring:message code="web.studentController.all.flatNumber" var="flatNumber"/>
<spring:message code="web.studentController.all.postalCode" var="postalCode"/>
<spring:message code="web.studentController.all.city" var="city"/>
<spring:message code="web.studentController.all.phoneNumber" var="phoneNumber"/>
<spring:message code="web.studentController.all.group" var="group"/>
<spring:message code="web.studentController.all.addStudent" var="add"/>

<div id="content">
    <p class="ptitle"> Uczeń: <a
            href="/studentProfile?id=${onlineUserFileMetaData.userId}">${student.surname} ${student.name}</a></p>
    <hr/>
    <p class="ptitle">Dane</p>
    <div id="userimage">
        <img src="/file${fileMetaData.uuidName}" alt="${fileMetaData.details}" width="168" height="168"/>
        <div style="clear: both;"></div>
        <c:if test="${onlineUserFileMetaData.userId == student.userId}">
            <sec:authorize access="hasRole('ROLE_USER')">
                <div class="button"><a href="/uploadUserPhoto" class="buttonlink">Zmień</a></div>
            </sec:authorize>
        </c:if>
    </div>
    <div id="userinfo">
        <table class="properties">
            <tr>
                <th class="dn">${surname}</th>
                <td colspan="1">${student.surname}</td>
                <th class="dn">${street}</th>
                <td colspan="1">${student.street}</td>
            </tr>
            <tr>
                <th class="dn">${name}</th>
                <td colspan="1">${student.name}</td>
                <th class="dn">${houseNumber}</th>
                <td colspan="1">${student.houseNumber}</td>
            </tr>
            <tr>
                <th class="dn">${pesel}</th>
                <td colspan="1">${student.pesel}</td>
                <th class="dn">${flatNumber}</th>
                <td colspan="1">${student.flatNumber}</td>
            </tr>
            <tr>
                <th class="dn">${phoneNumber}</th>
                <td colspan="1">${student.phoneNumber}</td>
                <th class="dn">${postalCode}</th>
                <td colspan="1">${student.postalCode}</td>
            </tr>
            <tr>
                <th class="dn">${group}</th>
                <td colspan="1"><a href="/group?groupName=${student.group}">${student.group}</a></td>
                <th class="dn">${city}</th>
                <td colspan="1">${student.city}</td>
            </tr>
        </table>
        <div style="clear: both;"></div>
        <c:if test="${onlineUserFileMetaData.userId == student.userId}">
            <sec:authorize access="hasRole('ROLE_USER')">
                <div class="button"><a href="/editStudent/<sec:authentication
            property="principal.username"></sec:authentication>/${student.userId}" class="buttonlink">Edytuj dane</a>
                </div>
                <div class="button"><a href="/removeStudent/<sec:authentication
            property="principal.username"></sec:authentication>/${student.userId}" class="buttonlink">Usuń profil</a>
                </div>
            </sec:authorize>
        </c:if>
    </div>
    <div style="clear: both;"></div>
    <hr/>
    <p class="ptitle">Oceny</p><br/>
    <table class="properties table-hover">
        <thead>
        <tr>
            <th class="gn">Lp</th>
            <th class="gn">Ocena</th>
            <th class="gn">Przedmiot</th>
            <th class="gn">Opis</th>
            <th class="gn">Data</th>
            <th class="gn">Nauczyciel</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${grades}" var="grade" varStatus="status">
            <tr>
                <td colspan="1">${status.index+1}</td>
                <td colspan="1"><a href="/gradeDetails/${grade.gradeId}">${grade.grade}</a></td>
                <td colspan="1">${grade.subject}</td>
                <td colspan="1">${grade.gradeDetail}</td>
                <td colspan="1">${grade.gradeDate}</td>
                <td colspan="1">${grade.teacherId}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <br/>
    <hr/>
    <p class="ptitle">Nauczyciele</p><br/>
    <table class="properties table-hover">
        <thead>
        <tr>
            <th class="gn">Lp</th>
            <th class="gn">Nazwisko i imię</th>
            <th class="gn">Przedmiot</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${allTeachers}" var="teacher" varStatus="status">
            <tr>
                <td colspan="1">${status.index+1}</td>
                <td colspan="1"><a
                        href="/teacherProfile?id=${teacher.userId}">${teacher.surname} ${teacher.name}</a></td>
                <td colspan="1">${teacher.subject}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
