<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="loginUrl"><c:url value="/login"/></c:set>
<spring:message code="web.mainController.loginContent.email" var="email1"/>
<spring:message code="web.mainController.loginContent.password" var="password1"/>
<spring:message code="web.mainController.loginContent.signIn" var="signIn1"/>

<div id="content">
    <div id="login">
        <form method="post" action="${loginUrl}">
            <input type="text" name="email" placeholder="${email1}" onfocus="this.placeholder=''"
                   onblur="this.placeholder='${email1}'" class="input-default"/>
            <input type="password" name="password" placeholder="${password1}" onfocus="this.placeholder=''"
                   onblur="this.placeholder='${password1}'" class="input-default"/>
            <span class="error">${error}</span>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <input type="submit" value="${signIn1}"/>
        </form>
    </div>
</div>
