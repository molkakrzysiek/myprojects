<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="content">
    <p class="ptitle">Klasa</p>
    <table class="properties table-hover">
        <thead>
        <tr>
            <th class="gn">Lp</th>
            <th class="gn">Nazwisko i imię</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${group}" var="student" varStatus="status">
            <tr>
                <td colspan="1">${status.index+1}</td>
                <td colspan="1"><a
                        href="/studentProfile?id=${student.userId}">${student.surname} ${student.name}</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>