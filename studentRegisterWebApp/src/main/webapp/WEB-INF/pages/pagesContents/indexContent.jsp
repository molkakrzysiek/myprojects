<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="content">

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et egestas lectus, eu feugiat ligula. Aliquam ut
    nunc vel sem elementum bibendum eu at leo. Aenean eget ornare odio, eget sagittis elit. Sed dapibus vel eros a
    sollicitudin. Sed ac mollis ligula, vitae dictum magna. Donec bibendum at massa sollicitudin rhoncus. Donec
    condimentum metus orci, ut maximus velit convallis id. Nunc ut lectus cursus, maximus eros id, placerat risus.
    In malesuada libero nec consectetur bibendum. Nullam est magna, consequat et sollicitudin finibus, gravida
    vestibulum massa. Vestibulum magna ante, semper mollis erat sed, ornare aliquet velit. Nullam convallis tortor
    ligula, at faucibus ante tristique quis.<br/><br/>

    Suspendisse potenti. Fusce hendrerit, lorem ut vulputate efficitur, risus purus consequat est, ut aliquam nulla
    mi vel ante. Donec vitae eros ac arcu molestie pretium. Praesent tortor quam, aliquet a ullamcorper sed, finibus
    non ante. Fusce in ex sit amet sem interdum volutpat. Donec vitae nunc eget nibh ultricies porttitor eu id
    ipsum. Aenean sagittis leo orci, eget interdum purus imperdiet pellentesque. Donec gravida nibh sed consequat
    imperdiet.<br/><br/>

    Suspendisse potenti. Fusce hendrerit, lorem ut vulputate efficitur, risus purus consequat est, ut aliquam nulla
    mi vel ante. Donec vitae eros ac arcu molestie pretium. Praesent tortor quam, aliquet a ullamcorper sed, finibus
    non ante. Fusce in ex sit amet sem interdum volutpat. Donec vitae nunc eget nibh ultricies porttitor eu id
    ipsum. Aenean sagittis leo orci, eget interdum purus imperdiet pellentesque. Donec gravida nibh sed consequat
    imperdiet.<br/><br/>

    Ut semper et libero vitae tincidunt. Sed vestibulum congue odio vel aliquet. Vivamus volutpat lectus ut mauris
    convallis, vitae egestas eros semper. Donec euismod pellentesque magna at imperdiet. Donec pulvinar lorem
    posuere maximus molestie. Praesent a convallis velit. Aliquam eu massa id metus dapibus pellentesque at sed mi.
    Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In malesuada
    tortor et elementum porta. Nunc quis facilisis neque, quis ullamcorper ante. Suspendisse a turpis in turpis
    tincidunt hendrerit.<br/><br/>

</div>
<div style="clear: both;"></div>

<div id="loginregistration">
    <div class="yt"><a href="/login" class="tilelink"><i class="icon-login"></i> <spring:message
            code="web.mainController.index.signIn"/></a></div>
    <div class="yt2"><a href="/registration" class="tilelink"><i class="icon-user-plus"></i> <spring:message
            code="web.mainController.index.signUp"/></a></div>
</div>

