<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="content">
    <p class="ptitle">Dodaj/zmień zdjęcie</p><br/>
    <br/>
    <form enctype="multipart/form-data" method="post" action="/upload">
        <p class="ptitle">Wybierz zdjęcie</p>
        <br/>
        <input type="file" name="file" />
        <br/>
        <br/><p class="ptitle">Opis zdjęcia</p>
        <input type="text" name="fileDetails" />
        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
        <br/>
        <input type="submit" value="Dodaj/Zmień"/>
    </form>
</div>
