<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<spring:message code="web.teacherController.all.surname" var="surname"/>
<spring:message code="web.teacherController.all.name" var="name"/>
<spring:message code="web.teacherController.all.subject" var="subject"/>

<div id="content">
    <p class="ptitle"> Nauczyciel: <a
            href="/teacherProfile?id=${onlineUserFileMetaData.userId}">${teacher.surname} ${teacher.name}</a></p>
    <hr/>
    <p class="ptitle">Dane</p>
    <div id="userimage">
        <img src="/file${fileMetaData.uuidName}" alt="${fileMetaData.details}" width="168" height="168"/>
        <div style="clear: both;"></div>
        <c:if test="${onlineUserFileMetaData.userId == teacher.userId}">
            <sec:authorize access="hasRole('ROLE_STAFF')">
                <div class="button"><a href="/uploadUserPhoto" class="buttonlink">Zmień</a></div>
            </sec:authorize>
        </c:if>
    </div>
    <div id="userinfo">
        <table class="properties">
            <tr>
                <th class="dn">${surname}</th>
                <td colspan="1">${teacher.surname}</td>
            </tr>
            <tr>
                <th class="dn">${name}</th>
                <td colspan="1">${teacher.name}</td>
            </tr>
            <tr>
                <th class="dn">${subject}</th>
                <td colspan="1">${teacher.subject}</td>
            </tr>
        </table>
        <div style="clear: both;"></div>
        <c:if test="${onlineUserFileMetaData.userId == teacher.userId}">
        <sec:authorize access="hasRole('ROLE_STAFF')">
            <div class="button"><a href="/editTeacher/<sec:authentication
            property="principal.username"></sec:authentication>/${teacher.userId}" class="buttonlink">Edytuj dane</a>
            </div>
        </sec:authorize>
        </c:if>
    </div>
    <div style="clear: both;"></div>
    <br/>
    <sec:authorize access="hasRole('ROLE_STAFF')">
        <hr/>
        <p class="ptitle">Oceny</p><br/>

        <div class="button" style="float: left;"><a href="/addGrade" class="buttonlink">Dodaj ocenę</a>
        </div>
    </sec:authorize>
    <br/><br/>
    <br/>
    <hr/>
    <p class="ptitle">Uczniowie</p>
    <table class="properties table-hover">
        <thead>
        <tr>
            <th class="gn">Lp</th>
            <th class="gn">Nazwisko i imię</th>
            <th class="gn">Klasa</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${allStudents}" var="student" varStatus="status">
            <tr>
                <td colspan="1">${status.index+1}</td>
                <td colspan="1"><a
                        href="/studentProfile?id=${student.userId}">${student.surname} ${student.name}</a></td>
                <td colspan="1"><a href="/group?groupName=${student.group}">${student.group}</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>