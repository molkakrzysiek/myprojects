<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<spring:message code="web.studentController.all.surname" var="surname"/>
<spring:message code="web.studentController.all.name" var="name"/>
<spring:message code="web.studentController.all.pesel" var="pesel"/>
<spring:message code="web.studentController.all.street" var="street"/>
<spring:message code="web.studentController.all.houseNumber" var="houseNumber"/>
<spring:message code="web.studentController.all.flatNumber" var="flatNumber"/>
<spring:message code="web.studentController.all.postalCode" var="postalCode"/>
<spring:message code="web.studentController.all.city" var="city"/>
<spring:message code="web.studentController.all.phoneNumber" var="phoneNumber"/>
<spring:message code="web.studentController.all.group" var="group"/>
<spring:message code="web.studentController.all.addStudent" var="add"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><spring:message code="web.studentController.addStudent.title"/> ${userEmail}</title>

    <link rel="stylesheet" href="<c:url value="/resources/css/style.css" />" type="text/css"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/fontello/fontello.css" />" type="text/css"/>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/blitzer/jquery-ui.css" type="text/css"/>
    <script src="https://code.jquery.com/jquery-3.1.0.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" type="text/javascript"></script>
</head>
<body>

<div id="container">
    <jsp:include page="/WEB-INF/pages/header.jsp">
        <jsp:param name="userPanel" value="true"/>
    </jsp:include>

    <div id="content">
        <div id="login">
            <form:form method="POST" modelAttribute="studentDTO">
                <form:input type="text" name="surname" path="surname" placeholder="${surname}"
                            onfocus="this.placeholder=''"
                            onblur="this.placeholder='${surname}'"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="surname"/></span></c:if>
                <form:input type="text" name="name" path="name" placeholder="${name}" onfocus="this.placeholder=''"
                            onblur="this.placeholder='${name}'"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="name"/></span></c:if>
                <form:input type="text" name="pesel" path="pesel" placeholder="${pesel}" onfocus="this.placeholder=''"
                            onblur="this.placeholder='${pesel}'"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error">${peselError} <form:errors
                        path="pesel"/></span></c:if>
                <form:input type="text" name="street" path="street" placeholder="${street}" onfocus="this.placeholder=''"
                            onblur="this.placeholder='${street}'"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="street"/></span></c:if>
                <form:input type="text" name="houseNumber" path="houseNumber" placeholder="${houseNumber}"
                            onfocus="this.placeholder=''"
                            onblur="this.placeholder='${houseNumber}'"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="houseNumber"/></span></c:if>
                <form:input type="text" name="flatNumber" path="flatNumber" placeholder="${flatNumber}"
                            onfocus="this.placeholder=''"
                            onblur="this.placeholder='${flatNumber}'"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="flatNumber"/></span></c:if>
                <form:input type="text" name="postalCode" path="postalCode" placeholder="${postalCode}"
                            onfocus="this.placeholder=''"
                            onblur="this.placeholder='${postalCode}'"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="postalCode"/></span></c:if>
                <form:input type="text" name="city" path="city" placeholder="${city}" onfocus="this.placeholder=''"
                            onblur="this.placeholder='${city}'"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="city"/></span></c:if>
                <form:input type="text" name="phoneNumber" path="phoneNumber" placeholder="${phoneNumber}"
                            onfocus="this.placeholder=''"
                            onblur="this.placeholder='${phoneNumber}'"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="phoneNumber"/></span></c:if>
                <form:input type="text" name="group" path="group" placeholder="${group}" onfocus="this.placeholder=''"
                            onblur="this.placeholder='${group}'"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="group"/></span></c:if>

                <input type="submit" value="${add}"/>
            </form:form>
        </div>
    </div>
    <jsp:include page="/WEB-INF/pages/footer.jsp"/>
</div>

</body>
</html>