<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><spring:message code="web.gradeController.editGrade.title"/></title>

    <link rel="stylesheet" href="<c:url value="/resources/css/style.css" />" type="text/css"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/fontello/fontello.css" />" type="text/css"/>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/blitzer/jquery-ui.css" type="text/css"/>
    <script src="https://code.jquery.com/jquery-3.1.0.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" type="text/javascript"></script>
</head>
<body>

<div id="container">
    <jsp:include page="/WEB-INF/pages/header.jsp">
        <jsp:param name="userPanel" value="true"/>
    </jsp:include>

    <div id="content">
        <p class="ptitle"> Ocena: </p>
        <hr/>

        <div id="userinfo">
            <form:form method="POST" modelAttribute="gradeDTO" id="editForm">
                <table class="properties">

                    <tr>
                        <th class="dn">Ocena</th>
                        <td colspan="1"><form:select path="grade" name="grade">
                            <form:option value="${grade.grade}" selected="selected">${grade.grade}</form:option>
                            <form:option value="0">0</form:option>
                            <form:option value="1">1</form:option>
                            <form:option value="2">2</form:option>
                            <form:option value="3">3</form:option>
                            <form:option value="4">4</form:option>
                            <form:option value="5">5</form:option>
                            <form:option value="6">6</form:option>
                        </form:select></td>
                    </tr>
                    <tr>
                        <th class="dn">Przedmiot</th>
                        <td colspan="1"><form:select path="subject" name="subject">
                            <form:option value="${grade.subject}" selected="selected">${grade.subject}</form:option>
                            <c:forEach items="${subjects}" var="subject" varStatus="status">
                                <form:option value="${subject.subjectName}">${subject.subjectName}</form:option>
                            </c:forEach>
                        </form:select></td>
                    </tr>
                    <tr>
                        <th class="dn">Opis</th>
                        <td colspan="1"><form:input type="text" name="gradeDetail" path="gradeDetail"
                                                    value="${grade.gradeDetail}"></form:input><c:if
                                test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                                path="gradeDetail"/></span></c:if></td>
                    </tr>
                    <tr>
                        <th class="dn">Nauczyciel</th>
                        <td colspan="1">${grade.teacherId}</td>
                    </tr>
                    <tr>
                        <th class="dn">Data</th>
                        <td colspan="1">${grade.gradeDate}</td>
                    </tr>
                </table>

            </form:form>
            <div class="button"><a href="#" class="buttonlink" onclick="document.getElementById('editForm').submit();">Zapisz</a>
            </div>
            <div style="clear: both;"></div>
        </div>

    </div>
    <jsp:include page="/WEB-INF/pages/footer.jsp"/>
</div>

</body>
</html>