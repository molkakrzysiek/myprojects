<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<spring:message code="web.teacherController.teacherProfile.title" var="title"/>
<jsp:include page="/WEB-INF/pages/template.jsp">
    <jsp:param name="content" value="teacherProfileContent"/>
    <jsp:param name="userPanel" value="true"/>
    <jsp:param name="title" value="${title}"/>
</jsp:include>