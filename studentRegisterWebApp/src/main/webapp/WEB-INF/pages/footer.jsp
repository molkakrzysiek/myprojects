<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div style="clear: both;"></div>
<div id="footer">&copy; aaa Sp. z o.o.  |  <a href="mailto:pomoc@e-dziennik.pl">pomoc@e-dziennik.pl</a>  |  +48 662 406
    699  |  <a href="/">e-dziennik.pl</a>  |  <a href="/?language=pl"><img src="/resources/img/pl.png" width="24"
                                                                       height="15" alt="EN"/></a>  |  <a
            href="/?language=en"><img src="/resources/img/en.png" width="24"
                                      height="15" alt="EN"/></a>
</div>
