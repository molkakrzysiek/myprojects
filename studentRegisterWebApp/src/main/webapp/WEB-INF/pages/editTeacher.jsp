<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<spring:message code="web.teacherController.all.editTeacher" var="editTeacher"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><spring:message code="web.teacherController.editTeacher.title"/> ${userEmail}</title>

    <link rel="stylesheet" href="<c:url value="/resources/css/style.css" />" type="text/css"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/fontello/fontello.css" />" type="text/css"/>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/blitzer/jquery-ui.css" type="text/css"/>
    <script src="https://code.jquery.com/jquery-3.1.0.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" type="text/javascript"></script>
</head>
<body>

<div id="container">
    <jsp:include page="/WEB-INF/pages/header.jsp">
        <jsp:param name="userPanel" value="true"/>
    </jsp:include>
    <div id="content">
        <div id="login">
            <form:form method="POST" modelAttribute="teacherDTO">
                <p class="ptitle">Nazwisko</p><form:input type="text" name="surname" path="surname"
                                                          value="${teacher.surname}"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="surname"/></span></c:if>
                <p class="ptitle">Imię</p><form:input type="text" name="name" path="name" value="${teacher.name}"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="name"/></span></c:if>
                <p class="ptitle">Przedmiot</p><form:input type="text" name="subject" path="subject"
                                                           value="${teacher.subject}"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="subject"/></span></c:if>

                <input type="submit" value="${editTeacher}"/>
            </form:form>
        </div>
    </div>
    <jsp:include page="/WEB-INF/pages/footer.jsp"/>
</div>

</body>
</html>