<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="header">
    <c:url var="logoutUrl" value="/logout"/>
    <form action="${logoutUrl}" id="logout" method="post">
        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
    </form>
    <div id="logo"><a href="/" class="homelink"><i class="icon-book"></i></a></div>
    <div id="title"><spring:message code="web.mainController.header.title" /></div>
    <c:if test="${param.userPanel == true}">
    <div id="user">
        <div id="userphoto">
            <img src="/file${onlineUserFileMetaData.uuidName}" alt="${onlineUserFileMetaData.details}" width="60"
                 height="60"/>
        </div>
        <div class="option">
            <a href="/profile" class="profile"><sec:authentication
                    property="principal.username"></sec:authentication></a><br/>
            <a href="#" class="logout" onclick="document.getElementById('logout').submit();"><i
                    class="icon-logout"></i> <spring:message code="web.mainController.header.logout" /></a>
        </div>
    </div>
    </c:if>
</div>
<div style="clear: both;"></div>
