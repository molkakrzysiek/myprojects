<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<spring:message code="web.mainController.registration.email" var="email1"/>
<spring:message code="web.mainController.registration.password" var="password"/>
<spring:message code="web.mainController.registration.repassword" var="repassword"/>
<spring:message code="web.mainController.registration.teacherRole" var="teacherRole"/>
<spring:message code="web.mainController.registration.studentRole" var="studentRole"/>
<spring:message code="web.mainController.registration.signUp" var="signUp"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><spring:message code="web.mainController.registration.title"/></title>

    <link rel="stylesheet" href="<c:url value="/resources/css/style.css" />" type="text/css"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/fontello/fontello.css" />" type="text/css"/>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/blitzer/jquery-ui.css" type="text/css"/>
    <script src="https://code.jquery.com/jquery-3.1.0.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" type="text/javascript"></script>
    <script src="<c:url value="/resources/checkUser.js"/>" type="text/javascript"></script>
</head>
<body>

<div id="container">
    <jsp:include page="/WEB-INF/pages/header.jsp">
        <jsp:param name="userPanel" value="false"/>
    </jsp:include>

    <div id="content">
        <div id="login">
            <form:form method="POST" modelAttribute="registrationFormDTO">
                <form:input type="text" id="email" name="email" path="email" placeholder="${email1}"
                            onfocus="this.placeholder=''"
                            onblur="this.placeholder='${email1}'" class="input-default"/>
                <span id="ajaxGetUserServletResponse"></span>
                <form:input type="password" id="password" name="password" path="password" placeholder="${password}"
                            onfocus="this.placeholder=''"
                            onblur="this.placeholder='${password}'" class="input-default"/>
                <form:input type="password" id="rePassword" name="rePassword" path="rePassword"
                            placeholder="${repassword}" onfocus="this.placeholder=''"
                            onblur="this.placeholder='${repassword}'" class="input-default"/>
                <span id="passwordResponse"></span>
                <label class="option">
                    <form:radiobutton path="role" id="user1" value="ROLE_USER"
                                      required="required" align="left"
                                      checked="checked"></form:radiobutton> ${studentRole}</label>
                <br/>
                <label class="option">
                    <form:radiobutton path="role" id="teacher" value="ROLE_STAFF"
                                      required="required" align="left"></form:radiobutton> ${teacherRole}</label>
                <input type="submit" value="${signUp}" disabled="disabled"/>
            </form:form>
        </div>
    </div>
    <jsp:include page="/WEB-INF/pages/footer.jsp"/>
</div>

</body>
</html>