<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<spring:message code="web.studentController.all.editStudent" var="editStudent"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><spring:message code="web.studentController.editStudent.title"/> ${userEmail}</title>

    <link rel="stylesheet" href="<c:url value="/resources/css/style.css" />" type="text/css"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/fontello/fontello.css" />" type="text/css"/>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/flick/jquery-ui.css" type="text/css"/>
</head>
<body>

<div id="container">
    <jsp:include page="/WEB-INF/pages/header.jsp">
        <jsp:param name="userPanel" value="true"/>
    </jsp:include>

    <div id="content">
        <div id="loginwide">
            <form:form method="POST" modelAttribute="studentDTO">
                <span><p class="ptitle">Nazwisko</p><form:input type="text" name="surname" path="surname"
                                                                value="${student.surname}"></form:input>
                </span><c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                    path="surname"/></span></c:if>
                <p class="ptitle">Imię</p><form:input type="text" name="name" path="name" value="${student.name}"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="name"/></span></c:if>
                <p class="ptitle">PESEL</p><form:input type="text" name="pesel" path="pesel" value="${student.pesel}"
                                                       readonly="true"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error">${peselError} <form:errors
                        path="pesel"/></span></c:if>
                <p class="ptitle">Ulica</p><form:input type="text" name="street" path="street"
                                                       value="${student.street}"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="street"/></span></c:if>
                <p class="ptitle">Nr domu</p><form:input type="text" name="houseNumber" path="houseNumber"
                                                         value="${student.houseNumber}"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="houseNumber"/></span></c:if>
                <p class="ptitle">Nr mieszkania</p><form:input type="text" name="flatNumber" path="flatNumber"
                                                               value="${student.flatNumber}"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="flatNumber"/></span></c:if>
                <p class="ptitle">Kod pocztowy</p><form:input type="text" name="postalCode" path="postalCode"
                                                              value="${student.postalCode}"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="postalCode"/></span></c:if>
                <p class="ptitle">Miasto</p><form:input type="text" name="city" path="city" value="${student.city}"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="city"/></span></c:if>
                <p class="ptitle">Telefon</p><form:input type="text" name="phoneNumber" path="phoneNumber"
                                                         value="${student.phoneNumber}"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="phoneNumber"/></span></c:if>
                <p class="ptitle">Klasa</p><form:input type="text" name="group" path="group" value="${student.group}"/>
                <c:if test="${pageContext.request.method=='POST'}"><span class="error"><form:errors
                        path="group"/></span></c:if>

                <input type="submit" value="${editStudent}"/>
            </form:form>
        </div>
    </div>

    <jsp:include page="/WEB-INF/pages/footer.jsp"/>
</div>

</body>
</html>
