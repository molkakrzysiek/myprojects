/**
 * Created by Krzysiek on 2016-07-19.
 */
$(document).ready(function () {

    var emailcheck = false;
    var passwordcheck = false;

    $('#email').keyup(function () {

        $('#ajaxGetUserServletResponse').empty();
        if (!isValidEmail($('#email').val())) {
            $('#email').removeClass().addClass('has-error');
            $('#ajaxGetUserServletResponse').text("Błędny adres email").addClass('option');
        } else {
            $.ajax({
                url: 'getUserServlet',
                data: {
                    email: $('#email').val()
                },
                success: function (responseText) {
                    createResponse(responseText);
                }
            });
        }
    });

    function isValidEmail(emailText) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailText);
    };

    function createResponse(response) {
        if (response == "true") {
            $('#email').removeClass().addClass('has-error');
            $('#ajaxGetUserServletResponse').text("Podany użytkownik istnieje!").addClass('option');
        } else {
            $('#email').removeClass().addClass('has-success');
            emailcheck = true;
            checkForm();
        }
    };

    $('input[type=password]').keyup(function () {
        var pass = $('#password'),
            repass = $('#rePassword');
        errorsInfo = "";
        $('#passwordResponse').empty();

        if (pass.val().length < 6 || pass.val().length > 20) {
            errorsInfo = errorsInfo + "Błędne hasło (wymagane 6-20 znaków).";
            pass.removeClass().addClass('has-error');
            repass.removeClass().addClass('has-error');
        } else {
            pass.removeClass().addClass('has-success');
            repass.removeClass().addClass(repass.val() === pass.val() ? 'has-success' : 'has-error');
            passwordcheck = repass.val() === pass.val() ? true : false;
            if (repass.val() != pass.val()) {
                errorsInfo = errorsInfo + " Hasła nie zgadzają się!";
            }
        }
        $('#passwordResponse').text(errorsInfo).addClass('option');
        checkForm();
    });

    function checkForm() {
        if (passwordcheck && emailcheck) {
            $("input[type=submit]").removeAttr("disabled");
        }
    }
});