package pl.molka.krzysztof.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.molka.krzysztof.dao.FileMetaDataDAO;
import pl.molka.krzysztof.dao.StudentDAO;
import pl.molka.krzysztof.dao.TeacherDAO;
import pl.molka.krzysztof.dao.UserDAO;
import pl.molka.krzysztof.dto.TeacherDTO;
import pl.molka.krzysztof.entity.Teacher;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Created by Krzysiek on 2016-07-01.
 */
@Controller
@Scope("session")
public class TeacherController {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private TeacherDAO teacherDAO;

    @Autowired
    private StudentDAO studentDAO;

    @Autowired
    private FileMetaDataDAO fileMetaDataDAO;

    @Secured({"ROLE_STAFF"})
    @RequestMapping("/addTeacher/{userEmail}/{userId}")
    public String addTeacher(Model model, HttpServletRequest request,
                             @ModelAttribute("teacherDTO") @Valid TeacherDTO teacherDTO, BindingResult result,
                             @PathVariable("userEmail") String userEmail, @PathVariable("userId") Long userId) {
        if (request.getMethod().equalsIgnoreCase("post") && !result.hasErrors()) {

            teacherDAO.createTeacher(createTeacherFromDTO(teacherDTO, userId));
            return "redirect:/profile?id=" + userId;
        }
        model.addAttribute("userEmail", userEmail);
        model.addAttribute("onlineUserFileMetaData", fileMetaDataDAO.getFileMetaDataByUserId(userId));
        return "addTeacher";
    }

    private Teacher createTeacherFromDTO(TeacherDTO teacherDTO, Long userId) {
        Teacher teacher = new Teacher();
        teacher.setUserId(userId);
        teacher.setSurname(teacherDTO.getSurname());
        teacher.setName(teacherDTO.getName());
        teacher.setSubject(teacherDTO.getSubject());
        return teacher;
    }

    @Secured({"ROLE_USER", "ROLE_STAFF"})
    @RequestMapping("/teacherProfile")
    public String teacherProfile(@RequestParam(value = "id") Long userId, Model model) {
        String userEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        Long onlineUserId = userDAO.getUserIdByEmail(userEmail);
        model.addAttribute("user", userDAO.getUserById(userId));
        model.addAttribute("teacher", teacherDAO.getTeacherByUserID(userId));
        model.addAttribute("allStudents", studentDAO.getAllStudentsOrderByGroup());
        model.addAttribute("fileMetaData", fileMetaDataDAO.getFileMetaDataByUserId(userId));
        model.addAttribute("onlineUserFileMetaData", fileMetaDataDAO.getFileMetaDataByUserId(onlineUserId));
        return "teacherProfile";
    }

    @Secured({"ROLE_STAFF"})
    @RequestMapping("/editTeacher/{userEmail}/{userId}")
    public String editTeacher(Model model, HttpServletRequest request,
                              @ModelAttribute("teacherDTO") @Valid TeacherDTO teacherDTO, BindingResult result,
                              @PathVariable("userEmail") String userEmail, @PathVariable("userId") Long userId) {

        Teacher teacher = teacherDAO.getTeacherByUserID(userId);
        model.addAttribute("teacher", teacher);
        model.addAttribute("userEmail", userEmail);
        model.addAttribute("onlineUserFileMetaData", fileMetaDataDAO.getFileMetaDataByUserId(userId));
        if (request.getMethod().equalsIgnoreCase("post") && !result.hasErrors()) {
            Teacher tempTeacher = createTeacherFromDTO(teacherDTO, userId);
            tempTeacher.setTeacherId(teacher.getTeacherId());
            teacherDAO.updateTeacher(tempTeacher);
            return "redirect:/teacherProfile?id=" + userId;
        }
        return "editTeacher";
    }
}
