package pl.molka.krzysztof.controllers;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.molka.krzysztof.entity.FileMetaData;
import pl.molka.krzysztof.dao.FileMetaDataDAO;
import pl.molka.krzysztof.dao.UserDAO;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Krzysiek on 2016-07-08.
 */
@Controller
@Scope("session")
public class DownloadController {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private FileMetaDataDAO fileMetaDataDAO;

    @RequestMapping(value = "/file/{fileName}", method = RequestMethod.GET)
    public void getFile(@PathVariable("fileName") String fileName, HttpServletResponse response) {
        try {
            downloadFileFromAS3(fileName, response);
        } catch (IOException ex) {
            System.out.println("Błąd przy pobieraniu pliku");
            response.setStatus(404);
        }
    }

    private void downloadFileFromAS3(@PathVariable("fileName") String fileName, HttpServletResponse response) throws IOException {
        String bucketName = "studentregister";
        String accessKey = "AKIAJV3MXL5DMT3AH6ZA";
        String secretKey = "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";
        FileMetaData metaData = fileMetaDataDAO.getFileMetaDataByUuidName(fileName);
        AmazonS3 s3client = new AmazonS3Client(new BasicAWSCredentials(accessKey, secretKey));
        S3Object object = s3client.getObject(new GetObjectRequest(bucketName, metaData.getUuidName()));
        InputStream is = object.getObjectContent();
        response.setContentType(metaData.getFileType());
        Long size = metaData.getFileSize();
        int size1 = size.intValue();
        response.setContentLength(size1);
        org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
        response.flushBuffer();
    }
}
