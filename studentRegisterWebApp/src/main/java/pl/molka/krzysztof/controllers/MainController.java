package pl.molka.krzysztof.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import pl.molka.krzysztof.dao.FileMetaDataDAO;
import pl.molka.krzysztof.entity.User;
import pl.molka.krzysztof.dao.UserDAO;
import pl.molka.krzysztof.dto.RegistrationFormDTO;

import java.util.Date;

/**
 * Created by Krzysiek on 2016-06-22.
 */

@Controller
@Scope("session")
public class MainController {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private FileMetaDataDAO fileMetaDataDAO;

    @RequestMapping("/")
    public String index(Model model) {
        return "index";
    }

    @RequestMapping("/login")
    public String login(Model model, @RequestParam(value = "error", required = false) String error) {
        if (error != null && error.equals("1")) {
            model.addAttribute("error", "Błędne dane użytkownika!");
        }
        return "login";
    }

    @RequestMapping("/registration")
    public String addUser(Model model, HttpServletRequest request, @ModelAttribute("registrationFormDTO")
    @Valid RegistrationFormDTO registrationFormDTO, BindingResult result) {
        if (request.getMethod().equalsIgnoreCase("post") && !result.hasErrors()) {
            userDAO.createOrUpdateUser(createUserFromDTO(registrationFormDTO));
            Long userId = userDAO.getUserIdByEmail(registrationFormDTO.getEmail());
            fileMetaDataDAO.createDefaultFileMetaData(userId);
            return "redirect:/add" + defineUserType(registrationFormDTO) + "/" + registrationFormDTO.getEmail() + "/" +
                    userId;
        }
        return "registration";
    }

    private User createUserFromDTO(RegistrationFormDTO registrationFormDTO) {
        User user = new User();
        user.setEmail(registrationFormDTO.getEmail());
        user.setPassword(registrationFormDTO.getPassword());
        user.setRole(registrationFormDTO.getRole());
        user.setEnabled(true);
        user.setRegistrationDate(new Date());
        return user;
    }

    private String defineUserType(RegistrationFormDTO registrationFormDTO) {
        if (registrationFormDTO.getRole().equals("ROLE_USER")) {
            return "Student";
        } else return "Teacher";
    }

    @RequestMapping("/profile")
    public String profile(Model model) {
        String userEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userDAO.getUserByEmail(userEmail);
        if (user.getRole().equals("ROLE_USER")) {
            return "redirect:/studentProfile?id=" + user.getUserId();
        } else return "redirect:/teacherProfile?id=" + user.getUserId();
    }

    @Secured({"ROLE_USER", "ROLE_STAFF"})
    @RequestMapping("/uploadUserPhoto")
    public String uploadUserPhoto(Model model) {
        String userEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        Long onlineUserId = userDAO.getUserIdByEmail(userEmail);
        model.addAttribute("onlineUserFileMetaData", fileMetaDataDAO.getFileMetaDataByUserId(onlineUserId));
        return "uploadUserPhoto";
    }
}
