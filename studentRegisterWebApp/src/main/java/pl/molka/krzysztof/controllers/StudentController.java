package pl.molka.krzysztof.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import pl.molka.krzysztof.*;
import pl.molka.krzysztof.dao.*;
import pl.molka.krzysztof.dto.StudentDTO;
import pl.molka.krzysztof.entity.Student;

/**
 * Created by Krzysiek on 2016-06-30.
 */
@Controller
@Scope("session")
public class StudentController {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private StudentDAO studentDAO;

    @Autowired
    private TeacherDAO teacherDAO;

    @Autowired
    private GradeDAO gradeDAO;

    @Autowired
    private FileMetaDataDAO fileMetaDataDAO;

    @Secured({"ROLE_USER"})
    @RequestMapping("/addStudent/{userEmail}/{userId}")
    public String addStudent(Model model, HttpServletRequest request,
                             @ModelAttribute("studentDTO") @Valid StudentDTO studentDTO, BindingResult result,
                             @PathVariable("userEmail") String userEmail, @PathVariable("userId") Long userId) {
        if (request.getMethod().equalsIgnoreCase("post") && !result.hasErrors()
                && new PeselValidator(studentDTO.getPesel()).isValid() && !studentDAO.isUserPeselExist(studentDTO.getPesel())) {
            studentDAO.createStudent(createStudentFromDTO(studentDTO, userId));
            return "redirect:/profile?id=" + userId;
        }

        if (request.getMethod().equalsIgnoreCase("post") && !new PeselValidator(studentDTO.getPesel()).isValid()) {
            model.addAttribute("peselError", "Błędny PESEL!");
        }
        if (request.getMethod().equalsIgnoreCase("post") && !studentDAO.isUserPeselExist(studentDTO.getPesel())) {
            model.addAttribute("peselError", "Użytkownik o podanym numerze PESEL już istnieje!");
        }
        model.addAttribute("userEmail", userEmail);
        model.addAttribute("onlineUserFileMetaData", fileMetaDataDAO.getFileMetaDataByUserId(userId));
        return "addStudent";
    }

    private Student createStudentFromDTO(StudentDTO studentDTO, Long userId) {
        Student student = new Student();
        student.setUserId(userId);
        student.setSurname(studentDTO.getSurname());
        student.setName(studentDTO.getName());
        student.setPesel(studentDTO.getPesel());
        student.setStreet(studentDTO.getStreet());
        student.setHouseNumber(studentDTO.getHouseNumber());
        student.setFlatNumber(studentDTO.getFlatNumber());
        student.setPostalCode(studentDTO.getPostalCode());
        student.setCity(studentDTO.getCity());
        student.setPhoneNumber(studentDTO.getPhoneNumber());
        student.setGroup(studentDTO.getGroup());
        return student;
    }

    @Secured({"ROLE_USER", "ROLE_STAFF"})
    @RequestMapping("/studentProfile")
    public String studentProfile(@RequestParam(value = "id") Long userId, Model model) {
        String userEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        Long onlineUserId = userDAO.getUserIdByEmail(userEmail);
        model.addAttribute("user", userDAO.getUserById(userId));
        model.addAttribute("student", studentDAO.getStudentByUserID(userId));
        model.addAttribute("grades", gradeDAO.getStudentGrades(studentDAO.getStudentByUserID(userId).getStudentId()));
        model.addAttribute("allTeachers", teacherDAO.getAllTeachersOrderBySurname());
        model.addAttribute("fileMetaData", fileMetaDataDAO.getFileMetaDataByUserId(userId));
        model.addAttribute("onlineUserFileMetaData", fileMetaDataDAO.getFileMetaDataByUserId(onlineUserId));
        return "studentProfile";
    }

    @Secured({"ROLE_USER"})
    @RequestMapping("/editStudent/{userEmail}/{userId}")
    public String editStudent(Model model, HttpServletRequest request,
                              @ModelAttribute("studentDTO") @Valid StudentDTO studentDTO, BindingResult result,
                              @PathVariable("userEmail") String userEmail, @PathVariable("userId") Long userId) {

        Student student = studentDAO.getStudentByUserID(userId);
        model.addAttribute("student", student);
        model.addAttribute("userEmail", userEmail);
        model.addAttribute("onlineUserFileMetaData", fileMetaDataDAO.getFileMetaDataByUserId(userId));
        if (request.getMethod().equalsIgnoreCase("post") && !result.hasErrors()) {
            Student tempStudent = createStudentFromDTO(studentDTO, userId);
            tempStudent.setStudentId(student.getStudentId());
            studentDAO.updateStudent(tempStudent);
            return "redirect:/studentProfile?id=" + userId;
        }
        return "editStudent";
    }

    @Secured({"ROLE_USER"})
    @RequestMapping("/removeStudent/{userEmail}/{userId}")
    public String removeStudent(Model model, @PathVariable("userEmail") String userEmail,
                                @PathVariable("userId") Long userId) {
        studentDAO.removeStudent(studentDAO.getStudentByUserID(userId));
        userDAO.removeUser(userDAO.getUserById(userId));
        return "index";
    }

    @Secured({"ROLE_USER", "ROLE_STAFF"})
    @RequestMapping("/group")
    public String showGroup(@RequestParam(value = "groupName", required = true) String groupName, Model model) {
        model.addAttribute("group", studentDAO.getGroup(groupName));
        String userEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        Long onlineUserId = userDAO.getUserIdByEmail(userEmail);
        model.addAttribute("onlineUserFileMetaData", fileMetaDataDAO.getFileMetaDataByUserId(onlineUserId));
        return "group";
    }
}
