package pl.molka.krzysztof.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.molka.krzysztof.dao.*;
import pl.molka.krzysztof.dto.GradeDTO;
import pl.molka.krzysztof.entity.Grade;
import pl.molka.krzysztof.entity.Teacher;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.DateFormat;
import java.util.Date;

/**
 * Created by Krzysiek on 2016-07-06.
 */
@Controller
@Scope("session")
public class GradeController {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private StudentDAO studentDAO;

    @Autowired
    private GradeDAO gradeDAO;

    @Autowired
    private TeacherDAO teacherDAO;

    @Autowired
    private SubjectDAO subjectDAO;

    @Autowired
    private FileMetaDataDAO fileMetaDataDAO;

    @Secured({"ROLE_USER", "ROLE_STAFF"})
    @RequestMapping("/gradeDetails/{gradeId}")
    public String gradeDetail(Model model, @PathVariable("gradeId") Long gradeId) {
        model.addAttribute("grade", gradeDAO.getGradeById(gradeId));
        String userEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        Long onlineUserId = userDAO.getUserIdByEmail(userEmail);
        model.addAttribute("onlineUserFileMetaData", fileMetaDataDAO.getFileMetaDataByUserId(onlineUserId));
        return "grade";
    }

    @Secured({"ROLE_STAFF"})
    @RequestMapping("/addGrade")
    public String addGrade(Model model, HttpServletRequest request,
                           @ModelAttribute("gradeDTO") @Valid GradeDTO gradeDTO, BindingResult result) {
        model.addAttribute("allStudents", studentDAO.getAllStudentsOrderBySurname());
        model.addAttribute("subjects", subjectDAO.getAllSubjects());
        String userEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        Long onlineUserId = userDAO.getUserIdByEmail(userEmail);
        model.addAttribute("onlineUserFileMetaData", fileMetaDataDAO.getFileMetaDataByUserId(onlineUserId));
        if (request.getMethod().equalsIgnoreCase("post") && !result.hasErrors()) {
            gradeDAO.createGrade(createNewGradeFromDTO(gradeDTO));
            return "redirect:/studentProfile?id=" + studentDAO.getUserIdByStudentId(gradeDTO.getStudentId());
        }
        return "addGrade";
    }

    private Grade createNewGradeFromDTO(GradeDTO gradeDTO) {
        String userEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        Long userId = userDAO.getUserIdByEmail(userEmail);
        Teacher teacher = teacherDAO.getTeacherByUserID(userId);
        Grade grade = new Grade();
        grade.setStudentId(gradeDTO.getStudentId());
        grade.setTeacherId(teacher.getTeacherId());
        grade.setGrade(gradeDTO.getGrade());
        grade.setGradeDate(gradeDTO.getGradeDate());
        grade.setGradeDetail(gradeDTO.getGradeDetail());
        grade.setSubject(gradeDTO.getSubject());
        return grade;
    }

    @Secured({"ROLE_STAFF"})
    @RequestMapping("/editGrade/{gradeId}")
    public String editGrade(Model model, HttpServletRequest request,
                            @ModelAttribute("gradeDTO") @Valid GradeDTO gradeDTO, BindingResult result,
                            @PathVariable("gradeId") Long gradeId) {
        Grade grade = gradeDAO.getGradeById(gradeId);
        model.addAttribute("grade", grade);
        model.addAttribute("subjects", subjectDAO.getAllSubjects());
        String userEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        Long onlineUserId = userDAO.getUserIdByEmail(userEmail);
        model.addAttribute("onlineUserFileMetaData", fileMetaDataDAO.getFileMetaDataByUserId(onlineUserId));
        if (request.getMethod().equalsIgnoreCase("post") && !result.hasErrors()) {
            Grade tempGrade = createEditedGradeFromDTO(gradeDTO, grade);
            gradeDAO.updateGrade(tempGrade);
            return "redirect:/gradeDetails/" + tempGrade.getGradeId();
        }
        return "editGrade";
    }

    private Grade createEditedGradeFromDTO(GradeDTO gradeDTO, Grade existGrade) {
        Grade tempGrade = new Grade();
        tempGrade.setGradeId(existGrade.getGradeId());
        tempGrade.setStudentId(existGrade.getStudentId());
        tempGrade.setTeacherId(existGrade.getTeacherId());
        tempGrade.setGradeDate(existGrade.getGradeDate());
        tempGrade.setGrade(gradeDTO.getGrade());
        tempGrade.setGradeDetail(gradeDTO.getGradeDetail());
        tempGrade.setSubject(gradeDTO.getSubject());
        return tempGrade;
    }
}
