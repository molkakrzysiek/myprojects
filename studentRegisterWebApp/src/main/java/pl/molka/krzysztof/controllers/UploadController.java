package pl.molka.krzysztof.controllers;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.NoResultException;

import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;
import pl.molka.krzysztof.entity.FileMetaData;
import pl.molka.krzysztof.dao.FileMetaDataDAO;
import pl.molka.krzysztof.dao.UserDAO;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;


/**
 * Created by Krzysiek on 2016-07-07.
 */
@Controller
public class UploadController {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private FileMetaDataDAO fileMetaDataDAO;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   @RequestParam("fileDetails") String fileDetails) {
        if (!file.isEmpty()) {
            try {
                String bucketName = "studentregister";
                String accessKey = "AKIAJV3MXL5DMT3AH6ZA";
                String secretKey = "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";
                FileMetaData fileMetaData = createOrUpdateFileMetaData(file, fileDetails);
                byte[] bytes = file.getBytes();
                InputStream inputStream = new ByteArrayInputStream(bytes);
                ObjectMetadata metadata = new ObjectMetadata();
                metadata.setContentLength(fileMetaData.getFileSize());
                metadata.setContentType(fileMetaData.getFileType());
                AmazonS3 s3client = new AmazonS3Client(new BasicAWSCredentials(accessKey, secretKey));
                s3client.putObject(new PutObjectRequest(
                        bucketName, fileMetaData.getUuidName(), inputStream, metadata));

                System.out.println("File has been successfully uploaded as " + fileMetaData.getOriginalFileName());
            } catch (AmazonServiceException ase) {
                System.out.println("Caught an AmazonServiceException, which " +
                        "means your request made it " +
                        "to Amazon S3, but was rejected with an error response" +
                        " for some reason.");
                System.out.println("Error Message:    " + ase.getMessage());
                System.out.println("HTTP Status Code: " + ase.getStatusCode());
                System.out.println("AWS Error Code:   " + ase.getErrorCode());
                System.out.println("Error Type:       " + ase.getErrorType());
                System.out.println("Request ID:       " + ase.getRequestId());
            } catch (AmazonClientException ace) {
                System.out.println("Caught an AmazonClientException, which " +
                        "means the client encountered " +
                        "an internal error while trying to " +
                        "communicate with S3, " +
                        "such as not being able to access the network.");
                System.out.println("Error Message: " + ace.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Uploaded file is empty");
        }
        return "redirect:/profile";
    }

    private FileMetaData createOrUpdateFileMetaData(MultipartFile file, String fileDetails) {
        UUID uuid = UUID.randomUUID();
        String filename = "/upload_" + uuid.toString();
        String userEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        Long userId = userDAO.getUserIdByEmail(userEmail);
        FileMetaData fileMetaData;
        try {
            fileMetaData = fileMetaDataDAO.getFileMetaDataByUserId(userId);
        } catch (NoResultException e) {
            fileMetaData = new FileMetaData();
        }
        fileMetaData.setUserId(userId);
        fileMetaData.setOriginalFileName(file.getOriginalFilename());
        fileMetaData.setFileSize(file.getSize());
        fileMetaData.setFileType(file.getContentType());
        fileMetaData.setUuidName(filename);
        fileMetaData.setDetails(fileDetails);
        fileMetaDataDAO.createFileMetaData(fileMetaData);
        return fileMetaData;
    }
}
