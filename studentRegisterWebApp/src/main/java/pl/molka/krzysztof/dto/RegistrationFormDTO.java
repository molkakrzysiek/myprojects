package pl.molka.krzysztof.dto;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Created by Krzysiek on 2016-06-23.
 */
public class RegistrationFormDTO {

    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    @Size(min = 6, max=20)
    private String password;

    @NotEmpty
    @Size(min = 6, max=20)
    private String rePassword;

    @NotEmpty
    @Size(min = 3)
    private String role;

    public RegistrationFormDTO() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRePassword() {
        return rePassword;
    }

    public void setRePassword(String rePassword) {
        this.rePassword = rePassword;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
