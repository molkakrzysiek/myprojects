package pl.molka.krzysztof.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Krzysiek on 2016-06-24.
 */
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID", unique = true, nullable = false)
    private Long userId;
    @Column(name = "USER_EMAIL", unique = true, nullable = false)
    private String email;
    @Column(name = "USER_PASSWORD", nullable = false)
    private String password;
    @Column(name = "USER_ENABLED", nullable = false)
    private Boolean enabled;
    @Column(name = "USER_ROLE", nullable = false)
    private String role;
    @Column(name = "REGISTRATION_DATE", nullable = false)
    private Date registrationDate;

    public User() {
    }

    public User(String email, String password, Boolean enabled, String role, Date registrationDate) {
        this.email = email;
        this.password = password;
        this.enabled = enabled;
        this.role = role;
        this.registrationDate = registrationDate;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long id) {
        this.userId = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }
}
