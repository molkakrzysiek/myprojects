package pl.molka.krzysztof.entity;

import javax.persistence.*;

@Entity
@Table(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STUDENT_ID", unique = true, nullable = false)
    private Long studentId;
    @Column(name = "USER_ID", unique = true, nullable = false)
    private Long userId;
    @Column(name = "SURNAME", nullable = false)
    private String surname;
    @Column(name = "NAME", nullable = false)
    private String name;
    @Column(name = "PESEL", unique = true, nullable = false)
    private String pesel;
    @Column(name = "STREET")
    private String street;
    @Column(name = "HOUSE_NUMBER")
    private Integer houseNumber;
    @Column(name = "FLAT_NUMBER")
    private Integer flatNumber;
    @Column(name = "POSTAL_CODE")
    private String postalCode;
    @Column(name = "CITY")
    private String city;
    @Column(name = "PHONE_NUMBER")
    private Integer phoneNumber;
    @Column(name = "STUDENT_GROUP")
    private String group;

    public Student() {
    }

    public Student(String group, Integer phoneNumber, String city, String postalCode, Integer flatNumber,
                   String street, Integer houseNumber, String pesel, String name, String surname, Long userId) {
        this.group = group;
        this.phoneNumber = phoneNumber;
        this.city = city;
        this.postalCode = postalCode;
        this.flatNumber = flatNumber;
        this.street = street;
        this.houseNumber = houseNumber;
        this.pesel = pesel;
        this.name = name;
        this.surname = surname;
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long id) {
        this.studentId = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(Integer houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Integer getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(Integer flatNumber) {
        this.flatNumber = flatNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
