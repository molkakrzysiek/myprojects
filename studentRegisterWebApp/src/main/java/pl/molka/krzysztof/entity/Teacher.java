package pl.molka.krzysztof.entity;

import javax.persistence.*;

/**
 * Created by Krzysiek on 2016-06-24.
 */
@Entity
@Table(name = "teachers")
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TEACHER_ID", unique = true, nullable = false)
    private Long teacherId;
    @Column(name = "USER_ID", unique = true, nullable = false)
    private Long userId;
    @Column(name = "SURNAME", nullable = false)
    private String surname;
    @Column(name = "NAME", nullable = false)
    private String name;
    @Column(name = "SUBJECT", nullable = false)
    private String subject;

    public Teacher() {
    }

    public Teacher(Long userId, String surname, String name, String subject) {
        this.userId = userId;
        this.surname = surname;
        this.name = name;
        this.subject = subject;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long id) {
        this.teacherId = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
