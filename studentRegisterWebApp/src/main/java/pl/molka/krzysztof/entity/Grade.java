package pl.molka.krzysztof.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Krzysiek on 2016-06-24.
 */
@Entity
@Table(name = "grades")
public class Grade {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "GRADE_ID", unique = true, nullable = false)
    private Long gradeId;
    @Column(name = "STUDENT_ID", nullable = false)
    private Long studentId;
    @Column(name = "TEACHER_ID", nullable = false)
    private Long teacherId;
    @Column(name = "GRADE", nullable = false)
    private Byte grade;
    @Column(name = "GRADE_DATE", nullable = false)
    private String gradeDate;
    @Column(name = "GRADE_DETAIL", nullable = false)
    private String gradeDetail;
    @Column(name = "SUBJECT", nullable = false)
    private String subject;

    public Grade() {
    }

    public Grade(Long studentId, Long teacherId, Byte grade, String gradeDate) {
        this.studentId = studentId;
        this.teacherId = teacherId;
        this.grade = grade;
        this.gradeDate = gradeDate;
    }

    public Long getGradeId() {
        return gradeId;
    }

    public void setGradeId(Long gradeId) {
        this.gradeId = gradeId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Byte getGrade() {
        return grade;
    }

    public void setGrade(Byte grade) {
        this.grade = grade;
    }

    public String getGradeDate() {
        return gradeDate;
    }

    public void setGradeDate(String gradeDate) {
        this.gradeDate = gradeDate;
    }

    public String getGradeDetail() {
        return gradeDetail;
    }

    public void setGradeDetail(String gradeDetail) {
        this.gradeDetail = gradeDetail;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
