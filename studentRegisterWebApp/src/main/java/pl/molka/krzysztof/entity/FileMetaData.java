package pl.molka.krzysztof.entity;

import javax.persistence.*;

/**
 * Created by Krzysiek on 2016-07-08.
 */
@Entity
@Table(name = "fileMetaData")
public class FileMetaData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;
    @Column(name = "USER_ID", unique = true, nullable = false)
    private Long userId;
    @Column(name = "ORIGINAL_FILE_NAME", nullable = false)
    private String originalFileName;
    @Column(name = "FILE_SIZE", nullable = false)
    private Long fileSize;
    @Column(name = "FILE_TYPE", nullable = false)
    private String fileType;
    @Column(name = "UUID_NAME", nullable = false)
    private String uuidName;
    @Column(name = "DETAILS")
    private String details;

    public FileMetaData() {
    }

    public FileMetaData(Long userId) {
        this.userId = userId;
        this.originalFileName = "user.png";
        this.fileSize = new Long(1408);
        this.fileType = "image/png";
        this.uuidName = "/upload_de246053-4039-420f-ae4a-0a03a7398435";
        this.details = "user photo";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getUuidName() {
        return uuidName;
    }

    public void setUuidName(String uuidName) {
        this.uuidName = uuidName;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
