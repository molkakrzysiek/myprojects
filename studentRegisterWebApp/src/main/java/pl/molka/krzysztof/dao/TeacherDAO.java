package pl.molka.krzysztof.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.molka.krzysztof.entity.Teacher;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import java.util.List;

/**
 * Created by Krzysiek on 2016-06-24.
 */
@Repository
public class TeacherDAO {
    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Teacher getTeacherByUserID(Long id) {
        Teacher teacher = null;
        try {
            Query query = entityManager.createQuery("SELECT k FROM Teacher k WHERE k.userId LIKE :custId").setParameter("custId", id);
            teacher = (Teacher) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return teacher;
    }

    @Transactional
    public List<Teacher> getAllTeachersOrderBySurname() {
        List<Teacher> teachers = null;
        try {
            Query query = entityManager.createQuery("SELECT t FROM Teacher t ORDER BY t.surname");
            teachers = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return teachers;
    }

    @Transactional
    public void createTeacher(Teacher teacher) {
        try {
            entityManager.merge(teacher);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void updateTeacher(Teacher teacher) {
        try {
            Query query = entityManager.createQuery("UPDATE Teacher k SET k.surname = :custSurname, k.name = :custName," +
                    "k.subject = :custSubject WHERE k.teacherId = :custTeacherId")
                    .setParameter("custSurname", teacher.getSurname()).setParameter("custName", teacher.getName())
                    .setParameter("custSubject", teacher.getSubject()).setParameter("custTeacherId", teacher.getTeacherId());
            query.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
