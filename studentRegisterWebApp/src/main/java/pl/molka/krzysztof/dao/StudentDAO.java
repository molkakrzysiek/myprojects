package pl.molka.krzysztof.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.molka.krzysztof.entity.Student;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import java.util.List;


/**
 * Created by Krzysiek on 2016-06-22.
 */
@Repository
public class StudentDAO {
    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Student getStudentByUserID(Long id) {
        Student student = null;
        try {
            Query query = entityManager.createQuery("SELECT k FROM Student k WHERE k.userId = :custId").setParameter("custId", id);
            student = (Student) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return student;
    }

    @Transactional
    public Long getUserIdByStudentId(Long id) {
        Long userId = null;
        try {
            Query query = entityManager.createQuery("SELECT k FROM Student k WHERE k.studentId = :custId").setParameter("custId", id);
            userId = ((Student) query.getSingleResult()).getUserId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userId;
    }

    @Transactional
    public List<Student> getAllStudentsOrderBySurname() {
        List<Student> students = null;
        try {
            Query query = entityManager.createQuery("SELECT k FROM Student k ORDER BY k.surname");
            students = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return students;
    }

    @Transactional
    public List<Student> getAllStudentsOrderByGroup() {
        List<Student> students = null;
        try {
            Query query = entityManager.createQuery("SELECT k FROM Student k ORDER BY k.group");
            students = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return students;
    }

    @Transactional
    public List<Student> getGroup(String groupName) {
        List<Student> students = null;
        try {
            Query query = entityManager.createQuery("SELECT k FROM Student k WHERE k.group = :custGroup ORDER BY k.surname")
                    .setParameter("custGroup", groupName);
            students = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return students;
    }

    @Transactional
    public Boolean isUserPeselExist(String pesel) {
        List<Student> results = null;
        try {
            Query query = entityManager.createQuery("SELECT k FROM Student k WHERE k.pesel = :custPesel").setParameter("custPesel", pesel);
            results = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (results.size() == 1) {
            return true;
        } else return false;
    }

    @Transactional
    public void createStudent(Student student) {
        try {
            entityManager.merge(student);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void updateStudent(Student student) {
        try {
            Query query = entityManager.createQuery("UPDATE Student k SET k.surname = :custSurname, k.name = :custName," +
                    "k.street = :custStreet, k.houseNumber = :custHouseNumber,k.flatNumber = :custFlatNumber," +
                    "k.postalCode = :custPostalCode, k.city = :custCity," +
                    "k.phoneNumber = :custPhoneNumber, k.group = :custGroup WHERE k.studentId = :custStudentId")
                    .setParameter("custSurname", student.getSurname()).setParameter("custName", student.getName())
                    .setParameter("custStreet", student.getStreet()).setParameter("custHouseNumber", student.getHouseNumber())
                    .setParameter("custFlatNumber", student.getFlatNumber()).setParameter("custPostalCode", student.getPostalCode())
                    .setParameter("custCity", student.getCity()).setParameter("custPhoneNumber", student.getPhoneNumber())
                    .setParameter("custGroup", student.getGroup()).setParameter("custStudentId", student.getStudentId());
            int updateCount = query.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void removeStudent(Student student) {
        try {
            entityManager.remove(entityManager.contains(student) ? student : entityManager.merge(student));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
