package pl.molka.krzysztof.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.molka.krzysztof.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import java.util.List;

/**
 * Created by Krzysiek on 2016-06-24.
 */
@Repository
public class UserDAO {
    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public User getUserById(Long id) {
        User user = null;
        try {
            entityManager.find(User.class, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    @Transactional
    public Long getUserIdByEmail(String email) {
        Long userId = null;
        try {
            Query query = entityManager.createQuery("SELECT k FROM User k WHERE k.email LIKE :custEmail").setParameter("custEmail", email);
            userId = ((User) query.getSingleResult()).getUserId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userId;
    }

    @Transactional
    public User getUserByEmail(String email) {
        User user = null;
        try {
            Query query = entityManager.createQuery("SELECT k FROM User k WHERE k.email = :custEmail").setParameter("custEmail", email);
            user = (User) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    @Transactional
    public void createOrUpdateUser(User user) {
        try {
            entityManager.merge(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public Boolean isUserExist(String email) {
        Query query = entityManager.createQuery("SELECT k FROM User k WHERE k.email LIKE :custEmail").setParameter("custEmail", email);
        List results = query.getResultList();
        if (results.size() == 1) {
            return true;
        } else return false;
    }

    @Transactional
    public void removeUser(User user) {
        try {
            entityManager.remove(entityManager.contains(user) ? user : entityManager.merge(user));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
