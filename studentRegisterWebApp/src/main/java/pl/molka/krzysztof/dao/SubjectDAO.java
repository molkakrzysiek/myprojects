package pl.molka.krzysztof.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.molka.krzysztof.entity.Subject;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import java.util.List;

/**
 * Created by Krzysiek on 2016-07-07.
 */
@Repository
public class SubjectDAO {

    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Subject getSubjectById(Long subjectId) {
        Subject subject = null;
        try {
            subject = entityManager.find(Subject.class, subjectId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subject;
    }

    @Transactional
    public List<Subject> getAllSubjects() {
        List<Subject> subjects = null;
        try {
            Query query = entityManager.createQuery("SELECT s FROM Subject s");
            subjects = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subjects;
    }

    @Transactional
    public void createSubject(Subject subject) {
        try {
            entityManager.merge(subject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void updateSubject(Subject subject) {
        try {
            Query query = entityManager.createQuery("UPDATE Subject s SET s.subjectName = :custSubjectName WHERE" +
                    " s.subjectId = :custSubjectId").setParameter("custSubjectName", subject.getSubjectName())
                    .setParameter("custSubjectId", subject.getSubjectId());
            int updateCount = query.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
