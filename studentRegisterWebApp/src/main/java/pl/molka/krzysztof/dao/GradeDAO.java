package pl.molka.krzysztof.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.molka.krzysztof.entity.Grade;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import java.util.List;

/**
 * Created by Krzysiek on 2016-06-24.
 */
@Repository
public class GradeDAO {
    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Grade getGradeById(Long gradeId) {
        Grade grade = null;
        try {
            grade = entityManager.find(Grade.class, gradeId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return grade;
    }

    @Transactional
    public List<Grade> getStudentGrades(Long studentId) {
        List<Grade> grades = null;
        try {
            Query query = entityManager.createQuery("SELECT g FROM Grade g WHERE g.studentId = :custId ORDER BY g.subject").setParameter("custId", studentId);
            grades = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return grades;
    }

    @Transactional
    public void createGrade(Grade grade) {
        try {
            entityManager.merge(grade);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void updateGrade(Grade grade) {
        try {
            Query query = entityManager.createQuery("UPDATE Grade g SET g.grade = :custGrade, g.gradeDetail = :custGradeDetail," +
                    "g.subject = :custSubject WHERE g.gradeId = :custGradeId")
                    .setParameter("custGrade", grade.getGrade()).setParameter("custGradeDetail", grade.getGradeDetail())
                    .setParameter("custSubject", grade.getSubject()).setParameter("custGradeId", grade.getGradeId());
            int updateCount = query.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
