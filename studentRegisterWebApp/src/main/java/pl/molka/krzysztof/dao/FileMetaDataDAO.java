package pl.molka.krzysztof.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.molka.krzysztof.entity.FileMetaData;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

/**
 * Created by Krzysiek on 2016-07-08.
 */
@Repository
public class FileMetaDataDAO {
    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public FileMetaData getFileMetaDataById(Long id) {
        FileMetaData fileMetaData;
        try {
            fileMetaData = entityManager.find(FileMetaData.class, id);
        } catch (Exception e) {
            e.printStackTrace();
            fileMetaData = new FileMetaData(0L);
        }
        return fileMetaData;
    }

    @Transactional
    public FileMetaData getFileMetaDataByUserId(Long userId) {
        FileMetaData fileMetaData;
        try {
            Query query = entityManager.createQuery("SELECT f FROM FileMetaData f WHERE f.userId = :custUserId")
                    .setParameter("custUserId", userId);
            fileMetaData = (FileMetaData) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            fileMetaData = new FileMetaData(0L);
        }
        return fileMetaData;
    }

    @Transactional
    public FileMetaData getFileMetaDataByUuidName(String uuidName) {
        FileMetaData fileMetaData;
        try {
            Query query = entityManager.createQuery("SELECT f FROM FileMetaData f WHERE f.uuidName = :custUuidName")
                    .setParameter("custUuidName", ("/"+uuidName));
            fileMetaData = (FileMetaData) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            fileMetaData = new FileMetaData(0L);
        }
        return fileMetaData;
    }

    @Transactional
    public void createFileMetaData(FileMetaData fileMetaData) {
        try {
            entityManager.merge(fileMetaData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void createDefaultFileMetaData(Long userId) {
        FileMetaData fileMetaData = new FileMetaData(userId);
        try {
            entityManager.merge(fileMetaData);
        } finally {

        }
    }

    @Transactional
    public void updateFileMetaData(FileMetaData fileMetaData) {
        try {
            Query query = entityManager.createQuery("UPDATE FileMetaData f SET f.originalFileName = :custOriginalFileName, " +
                    "f.fileSize = :custFileSize, f.fileType = :custFileType, f.uuidName = :custUuidName," +
                    "f.details = :custDetails WHERE f.id = :custId")
                    .setParameter("custOriginalFileName", fileMetaData.getOriginalFileName())
                    .setParameter("custFileSize", fileMetaData.getFileSize())
                    .setParameter("custFileType", fileMetaData.getFileType())
                    .setParameter("custUuidName", fileMetaData.getUuidName())
                    .setParameter("custId", fileMetaData.getId());
            int updateCount = query.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
