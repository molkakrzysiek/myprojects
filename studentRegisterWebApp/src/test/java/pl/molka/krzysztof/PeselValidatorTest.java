package pl.molka.krzysztof;

import junit.framework.TestCase;

/**
 * Created by Krzysiek on 2016-07-21.
 */
public class PeselValidatorTest extends TestCase {

    public void testIsValid() {
        PeselValidator peselValidator = new PeselValidator("01240604379");
        assertTrue(peselValidator.isValid());
        PeselValidator peselValidator2 = new PeselValidator("90041514679");
        assertTrue(peselValidator2.isValid());

        PeselValidator peselValidator3 = new PeselValidator("01240604378");
        assertFalse(peselValidator3.isValid());
        PeselValidator peselValidator4 = new PeselValidator("90041514671");
        assertFalse(peselValidator4.isValid());
    }
}
