$(document).ready(function () {

    $("#keywords").tagsinput({
                typeahead: {
                    hint: true,
                    highlight: true,
                    minLength: 1,
                    afterSelect: function(val) { this.$element.val(""); },
                    source: function(query) {
                                return $.get("/keywords");
                            }
                }
    });

    $('#sort, #filter').click(function(){
        var newUrl = "/campaigns?s=" + $('#sortBy').val() + "&o=" + $('#order').val() + "&f="+ $('#inputfilter').val();
        window.location.replace(newUrl); 
    });
});