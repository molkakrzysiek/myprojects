package models;

import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.validation.*;

@Entity 
public class Town extends Model {

    private static final long serialVersionUID = 1L;

	@Id
    public Long id;
    
    @Constraints.Required
    public String name;
    
    public static Model.Finder<Long,Town> find = new Model.Finder<Long,Town>(Long.class, Town.class);

    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(Town t: Town.find.orderBy("name").findList()) {
            options.put(t.id.toString(), t.name);
        }
        return options;
    }
}