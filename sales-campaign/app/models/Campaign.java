package models;

import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;

import com.avaje.ebean.*;

@Entity 
public class Campaign extends Model {

    private static final long serialVersionUID = 1L;

	@Id
    public Long id;
    
    @Constraints.Required
    public String name;
    
    @Constraints.Required
    public String keywords;
    
    @Constraints.Required
    @Constraints.Min(value=500)
    public Integer bidAmount;
    
    @Constraints.Required
    @Constraints.Min(value=0)
    public Integer campaignFund;
    
    @Constraints.Required
    public String status;
    
    @ManyToOne
    public Town town;
    
    @Constraints.Required
    @Constraints.Min(value=0)
    public Integer radius;
    
    public static Finder<Long,Campaign> find = new Finder<Long,Campaign>(Long.class, Campaign.class); 
    
    public static List<Campaign> allCampaigns(String sortBy, String order, String filter){
        return find.where().ilike("name", "%" + filter + "%").orderBy(sortBy + " " + order).fetch("town").findList();
    }
}