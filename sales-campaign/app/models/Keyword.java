package models;

import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.validation.*;

@Entity 
public class Keyword extends Model {

    private static final long serialVersionUID = 1L;

	@Id
    public Long id;
    
    @Constraints.Required
    public String word;
    
    public static Finder<Long,Keyword> find = new Finder<Long,Keyword>(Long.class, Keyword.class);

    public static List<String> getKeywords() {
        List<String> options = new ArrayList<String>();
        for(Keyword k: Keyword.find.where().orderBy("word").findList()) {
            options.add(k.word);
        }
        return options;
    }

    public static void add(String newKeywords){
        for (String newKeyword: newKeywords.split(",")){
            if(!checkIfExist(newKeyword)){
                Keyword keyword = new Keyword();
                keyword.word = newKeyword;
                keyword.save();
            }
        }    
    }

    public static Boolean checkIfExist(String keyword){
        if(Keyword.find.where().ieq("word", keyword).findList().size() == 0){
            return false;
        }
        else {
            return true;
        }
        }
}