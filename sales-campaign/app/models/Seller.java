package models;

import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;

import com.avaje.ebean.*;

@Entity 
public class Seller extends Model {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    public Long id;
    
    @Constraints.Required
    public String name;
    
    @Constraints.Required
    public String surname;
    
    @Constraints.Required
    public Integer fund;
    
    public static Finder<Long,Seller> find = new Finder<Long,Seller>(Long.class, Seller.class); 
    
    public static void reduceFund(Integer cost){
        Seller seller = find.byId(1L);
        Integer newFund = seller.fund - cost;
        seller.fund = newFund;
        seller.save();
    }
    
    public static void increaseFund(Integer cost){
        Seller seller = find.byId(1L);
        Integer newFund = seller.fund + cost;
        seller.fund = newFund;
        seller.save();
    }
}