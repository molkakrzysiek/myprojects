package controllers;

import play.mvc.*;
import play.data.*;
import static play.data.Form.*;
import play.libs.*;
import play.libs.F.Option;
import views.html.*;

import models.*;

public class Application extends Controller {
    
    public static Result GO_HOME = redirect(
        routes.Application.list("id","asc",""));
    
    public static Result index() {
        return GO_HOME;
    }

    public static Result list(String sortBy, String order, String filter) {
        return ok(campaignList.render(Campaign.allCampaigns(sortBy, order, filter), Seller.find.byId(1L)));
    }
    
    public static Result edit(Long id) {
        Form<Campaign> campaignForm = form(Campaign.class).fill(Campaign.find.byId(id));
        return ok(editCampaign.render(id, campaignForm, Seller.find.byId(1L)));
    }
    
    public static Result update(Long id) {
        Integer currentCampaignFund = Campaign.find.byId(id).campaignFund;
        Form<Campaign> campaignForm = form(Campaign.class).bindFromRequest();
        if(campaignForm.hasErrors()) {
            return badRequest(editCampaign.render(id, campaignForm, Seller.find.byId(1L)));
        }
        campaignForm.get().update(id);
        flash("success", "Campaign " + campaignForm.get().name + " has been updated");
        Seller.reduceFund(campaignForm.get().campaignFund - currentCampaignFund);
        Keyword.add(campaignForm.get().keywords);
        return GO_HOME;
    }
    
    public static Result create() {
        Form<Campaign> campaignForm = form(Campaign.class);
        return ok(createCampaign.render(campaignForm, Seller.find.byId(1L)));
    }
    
    public static Result save() {
        Form<Campaign> campaignForm = form(Campaign.class).bindFromRequest();
        if(campaignForm.hasErrors()) {
            return badRequest(createCampaign.render(campaignForm, Seller.find.byId(1L)));
        }
        campaignForm.get().save();
        flash("success", "Campaign " + campaignForm.get().name + " has been created");
        Seller.reduceFund(campaignForm.get().campaignFund);
        Keyword.add(campaignForm.get().keywords);
        return GO_HOME;
    }
    
    public static Result delete(Long id) {
        Seller.increaseFund(Campaign.find.byId(id).campaignFund);
        Campaign.find.ref(id).delete();
        flash("success", "Campaign has been deleted");
        return GO_HOME;
    }
    
    public static Result getAllKeywords() {
        return ok(Json.toJson(Keyword.getKeywords()));
    }
}