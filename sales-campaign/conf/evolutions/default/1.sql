# --- First database schema

# --- !Ups
create table seller (
  id                        bigint not null IDENTITY(1,1),
  name                      varchar(255),
  surname                   varchar(255),
  fund                      int,
  constraint pk_seller primary key (id))
;

create table keyword (
  id                        bigint not null IDENTITY(1,1),
  word                      varchar(255),
  constraint pk_keyword primary key (id))
;

create table town (
  id                        bigint not null IDENTITY(1,1),
  name                      varchar(255),
  constraint pk_town primary key (id))
;

create table campaign (
  id                        bigint not null IDENTITY(1,1),
  name                      varchar(255),
  keywords                  varchar(255),
  bid_amount                int,
  campaign_fund             int,
  status                    varchar(255),
  town_id                   bigint,
  radius                    int,
  constraint pk_campaign primary key (id))
;

create sequence seller_seq start with 100;
create sequence keyword_seq start with 100;
create sequence town_seq start with 100;
create sequence campaign_seq start with 100;

alter table campaign add constraint fk_campaign_town_1 foreign key (town_id) references town (id) on delete restrict on update restrict;

# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists seller;
drop table if exists keyword;
drop table if exists town;
drop table if exists campaign;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists seller_seq;
drop sequence if exists keyword_seq;
drop sequence if exists town_seq;
drop sequence if exists campaign_seq;
