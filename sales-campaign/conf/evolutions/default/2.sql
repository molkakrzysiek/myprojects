# --- Sample dataset

# --- !Ups
insert into seller (id,name,surname,fund) values (1,'Allan','Smith',1000000);

insert into keyword (id,word) values (1,'advertisement');
insert into keyword (id,word) values (2,'art');
insert into keyword (id,word) values (3,'birth');
insert into keyword (id,word) values (4,'body');
insert into keyword (id,word) values (5,'car');
insert into keyword (id,word) values (6,'day');
insert into keyword (id,word) values (7,'detail');
insert into keyword (id,word) values (8,'earth');
insert into keyword (id,word) values (9,'example');
insert into keyword (id,word) values (10,'free');
insert into keyword (id,word) values (11,'flower');
insert into keyword (id,word) values (12,'glass');
insert into keyword (id,word) values (13,'group');
insert into keyword (id,word) values (14,'history');
insert into keyword (id,word) values (15,'help');
insert into keyword (id,word) values (16,'the best');
insert into keyword (id,word) values (17,'idea');
insert into keyword (id,word) values (18,'instrument');
insert into keyword (id,word) values (19,'journey');
insert into keyword (id,word) values (20,'jump');
insert into keyword (id,word) values (21,'key');
insert into keyword (id,word) values (22,'king');
insert into keyword (id,word) values (23,'land');
insert into keyword (id,word) values (24,'language');
insert into keyword (id,word) values (25,'money');
insert into keyword (id,word) values (26,'new');
insert into keyword (id,word) values (27,'opinion');
insert into keyword (id,word) values (28,'play');
insert into keyword (id,word) values (29,'quality');
insert into keyword (id,word) values (30,'range');
insert into keyword (id,word) values (31,'sand');
insert into keyword (id,word) values (32,'size');
insert into keyword (id,word) values (33,'test');
insert into keyword (id,word) values (34,'top');
insert into keyword (id,word) values (35,'unit');
insert into keyword (id,word) values (36,'value');
insert into keyword (id,word) values (37,'water');
insert into keyword (id,word) values (38,'xerox');
insert into keyword (id,word) values (39,'year');
insert into keyword (id,word) values (40,'zinc');

insert into town (id,name) values (1,'Londyn');
insert into town (id,name) values (2,'Birmingham');
insert into town (id,name) values (3,'Glasgow');
insert into town (id,name) values (4,'Leeds');
insert into town (id,name) values (5,'Manchester');
insert into town (id,name) values (6,'Bristol');
insert into town (id,name) values (7,'Liverpool');
insert into town (id,name) values (8,'Sheffield');
insert into town (id,name) values (9,'Edynburg');
insert into town (id,name) values (10,'Aberdeen');
insert into town (id,name) values (11,'Belfast');
insert into town (id,name) values (12,'Bolton');
insert into town (id,name) values (13,'Bradford');
insert into town (id,name) values (14,'Cambridge');
insert into town (id,name) values (15,'Northampton');
insert into town (id,name) values (16,'Reading');
insert into town (id,name) values (17,'Southampton');
insert into town (id,name) values (18,'Sunderland');
insert into town (id,name) values (19,'Wolverhampton');
insert into town (id,name) values (20,'York');

insert into campaign (id,name,keywords,bid_amount,campaign_fund,status,town_id,radius) values (  1,'Campaign 1','new',1000,500,'on',1,50);
insert into campaign (id,name,keywords,bid_amount,campaign_fund,status,town_id,radius) values (  2,'Campaign 2','car',3000,1000,'on',5,20);
insert into campaign (id,name,keywords,bid_amount,campaign_fund,status,town_id,radius) values (  3,'Campaign new','key',4000,2500,'on',1,120);
insert into campaign (id,name,keywords,bid_amount,campaign_fund,status,town_id,radius) values (  4,'Campaign 3','money',5000,200,'off',8,300);
insert into campaign (id,name,keywords,bid_amount,campaign_fund,status,town_id,radius) values (  5,'Camp','jump',1000,700,'on',9,40);
insert into campaign (id,name,keywords,bid_amount,campaign_fund,status,town_id,radius) values (  6,'Abcd','day',3200,800,'on',20,550);
insert into campaign (id,name,keywords,bid_amount,campaign_fund,status,town_id,radius) values (  7,'Qwerty','play',8200,1000,'off',5,100);
insert into campaign (id,name,keywords,bid_amount,campaign_fund,status,town_id,radius) values (  8,'Look','zinc',2330,5000,'off',19,700);
insert into campaign (id,name,keywords,bid_amount,campaign_fund,status,town_id,radius) values (  9,'Book','range',2130,6000,'off',17,570);
insert into campaign (id,name,keywords,bid_amount,campaign_fund,status,town_id,radius) values (  10,'One','size',2930,4100,'on',7,450);
insert into campaign (id,name,keywords,bid_amount,campaign_fund,status,town_id,radius) values (  11,'Two','test',5390,2300,'on',9,670);
insert into campaign (id,name,keywords,bid_amount,campaign_fund,status,town_id,radius) values (  12,'Five','top',1030,5400,'off',10,140);
insert into campaign (id,name,keywords,bid_amount,campaign_fund,status,town_id,radius) values (  13,'Six','unit',7130,1600,'on',11,200);
insert into campaign (id,name,keywords,bid_amount,campaign_fund,status,town_id,radius) values (  14,'Car campaign','new',5030,65800,'off',18,150);
insert into campaign (id,name,keywords,bid_amount,campaign_fund,status,town_id,radius) values (  15,'This','value',1230,12300,'off',15,90);
# --- !Downs

delete from campaign;
delete from town;
